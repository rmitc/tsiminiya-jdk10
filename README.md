# Java Application Builder and Runner

This is a simple example of a Docker image which allows building and running of a Java Project with an executable main class.


## Image Structure

### Volumes
| Location              | Description                                                       |
|-----------------------|-------------------------------------------------------------------|
| /usr/javaproject      | Location of Java Project to build                                 |
| /usr/javabuildscript  | Location of build scripts to run                                  |
| /usr/javaapp          | Location of build folder containing .class files and/or JAR files |

### Environment Variables
| Variable | Description                                |
|----------|--------------------------------------------|
| APP_MAIN | The qualified class name of the main class | 


## How to use this image

### Docker CLI
By default, if you run the following command:
```bash
docker run -it tsiminiya/jdk10
```
you will see the string "Hello World!" printed in the terminal.

But for example: you have a project _date-time-printer_, and you want to use the image to build and run. Your Docker CLI command may look like something like the following:

```bash
cd date-time-printer
docker run -it -v ${PWD}:/usr/javaproject -e APP_MAIN="sample.DateTimePrinterMain" tsiminiya/jdk10
```

#### Notes
1. The segment _-v ${PWD}:/usr/javaproject_ directs Docker to mount the current working directory to the volume _/usr/javaproject_. In effect, the contents of the current directory (in the host) will be the location of the Java Project to be build.
2. The segment _-e APP_MAIN="sample.DateTimePrinterMain"_ tells Docker that the environment variable _APP_MAIN_ will have a value of _"sample.DateTimePrinterMain"_ or the qualified main class of the Java Project.


### Docker Compose
The Docker Compose equivalent of the Docker CLI command above should be something like the following:

```yaml
version: 3
services:
  datetimeprinter:
    image: tsiminiya/jdk10
    volumes:
      - ${PWD}:/usr/javaproject
    environment:
      APP_MAIN: sample.DateTimePrinterMain
```


For more Docker samples please visit http://www.rm-itc.com
