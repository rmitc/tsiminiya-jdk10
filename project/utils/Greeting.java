package utils;

public class Greeting {
    
    private String message;

    public Greeting(String message) {
        this.message = message;
    }

    public void show() {
        System.out.println(message);
    }

}