#1 Base Image
FROM ubuntu:16.04

#2 Install Java
RUN mkdir -p /usr/java
COPY jdk-10.0.2_linux-x64_bin.tar.gz /usr/java
RUN tar xvf /usr/java/jdk-10.0.2_linux-x64_bin.tar.gz -C /usr/java
ENV JAVA_HOME=/usr/java/jdk-10.0.2
ENV PATH=${PATH}:/usr/java/jdk-10.0.2/bin

#3 Create Java Project Folder
RUN mkdir -p /usr/javaproject

#4 Create Java Build Folder
RUN mkdir -p /usr/javaapp

#5 Create Java Build Script Folder
RUN mkdir -p /usr/javabuildscript

#6 Create Location Folder for the executor of build scripts
RUN mkdir -p /usr/mainscript

#7 Copy folder project to Java Project Folder
ADD project /usr/javaproject

#8 Copy build-and-run.sh
COPY build-and-run.sh /usr/javabuildscript

#9 Copy execute-scripts.sh to /usr/mainscript
COPY execute-scripts.sh /usr/mainscript

#10 Set working directory
WORKDIR /usr/javaapp

#11 Create mountable directories or volumes
VOLUME [ "/usr/javaproject", "/usr/javaapp", "/usr/javabuildscript" ]

#12 Execute app.jar
CMD [ "sh", "/usr/mainscript/execute-scripts.sh" ]
