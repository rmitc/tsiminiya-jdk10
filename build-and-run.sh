#! /bin/sh

if [ -z "$APP_MAIN" ]; then
    APP_MAIN="HelloWorld"
fi

echo "Compiling Java files..."
find /usr/javaproject -name "*.java" -print | xargs javac -d /usr/javaapp
echo "Creating app.jar..."
jar cvfe app.jar $APP_MAIN .
echo "Running app.jar..."
echo "==================== Tsiminiya/JDK10 ====================\n\n\n"
java -jar /usr/javaapp/app.jar
